# Asus G73Jh led, buttons and fan control support #

This is a patch for the asus-laptop kernel module to add extra support for the Asus G73Jh laptop. It adds the following functionality:

* Set on/off fancy lighbar under screen
* Set on/off led under buttons in top left corner of laptop
* All buttons in top left corner now has keycodes
* The fan speed can be read and controlled for both the fans, separately

While this code works on my computer I cannot take responsibility for whatever happens to your computer. Calling the same ACPI functions on another model, or even just another BIOS version, can have unforeseen consequences. Especially **stopping or throttling your fans can damage your hardware**.

## Known bugs ##

* The current brightness level (on/off) cannot be read for either of the lights. It is possible to read the lightbar brightness using another write function, but then the keyboard backlight is also affected. This is documented in the code.
* After using manual fan mode and setting it back to "automatic" it will not behave like before, it tends to switch between minimum speed and max speed which is disturbing. A reboot solves this problem, and if you are poking the fan speed values you probably want something like fancontrol anyway.



## Installation ##
I use dkms to build this automatically everytime the kernel is updated. Depending on linux distro you might want to do this differently. 

The following should work on *buntu, ymmv

1. sudo apt-get install dkms build-essentials
1. cd /tmp
1. wget https://bitbucket.org/mgulin/asus-laptop/get/master.zip
1. unzip master.zip
1. sudo mkdir /usr/src/asus-laptop-0.1
1. cd mgulin-asus-laptop-*/ && sudo mv Makefile dkms.conf asus-laptop.c /usr/src/asus-laptop-0.1/
1. cd .. && rm -rf mgulin-asus-laptop-*/ master.zip
1. sudo dkms add -m asus-laptop -v 0.1
1. sudo dkms build -m asus-laptop -v 0.1
1. sudo dkms install -m asus-laptop -v 0.1


## Credits ##

* The original authors of asus-laptop (and everybody else who has contributed to it)
* [prikolchik](http://forum.notebookreview.com/threads/fan-control-on-asus-prime-ux31-ux31a-ux32a-ux32vd.705656/) for a nice writeup of some of the functions found in the Asus acpi tables
* [chr](http://sourceforge.net/p/acpi4asus/mailman/message/7375427/) for his Asus hwmon patch